<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//Usuário
Route::get('/editar/perfil', 'UserController@getUserById')->name('editarPerfil');
Route::post('/atualizar/perfil', 'UserController@update')->name('atualizarPerfil');
Route::get('/listar/servicos', 'ServicesController@listServices')->name('listarServ icos');
Route::get('/virar/autonomo/{id}', 'UserController@turnAutonomous')->name('virarAutonomo');
Route::post('/virar/autonomo', 'UserController@updateToAutonomous')->name('atualizarParaAutonomo');

//Serviço
Route::get('/registrar/servico', 'UserController@createServece')->name('cirarServico');
Route::get('/listar/servicos', 'ServicesController@listServices')->name('listarServicos');
Route::get('/cadastrar/servicos', 'ServicesController@createService')->name('cadastrarServico');
Route::post('/cadastrar/servicos', 'ServicesController@storeServices')->name('storeServices');
Route::get('/atualizar/servico', 'ServicesController@getServiceById')->name('atualizarServico');
Route::post('/atualizar/servico/{id}', 'ServicesController@update')->name('updateServico');


// Agendamento
Route::get('/servicos/lista', 'ServicesController@listaServicos')->name('listaServicos');
Route::get('/autonomos/sericos', 'UserController@getAutonomoByServiceId')->name('getAutonomoByServiceId');
Route::get('/horario/autonomo', 'UserController@getSchedules')->name('horariosAutonomo');
Route::post('/agendamento', 'ScheduleController@create')->name('criarAgendamento');