<?php

namespace App\Services;

use App\Repositories\ScheduleRepository;

class ScheduleServices
{
    protected $scheduleRepository;

    public function __construct(ScheduleRepository $scheduleRepository)
    {
        $this->scheduleRepository = $scheduleRepository;
    }

    public function create($data)
    {
        return $this->scheduleRepository->create($data);
    }

    public function getScheduleByUserId($userId)
    {
        return $this->scheduleRepository->getByUserId($userId);
    }

    public function getScheduleByAutonomousId($userId)
    {
        return $this->scheduleRepository->getScheduleByAutonomousId($userId);
    }
}