<?php

namespace App\Http\Controllers;

use App\Services\ServicesServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ServicesController extends Controller
{
    protected $servicesService;

    public function __construct(ServicesServices $servicesServices)
    {
        $this->servicesService = $servicesServices;
    }

    public function listServices()
    {
        $services = $this->servicesService->getAllByUserId(Auth::id());
        return view('services.index', ['services' => $services, 'user'=> Auth::user()]);
    }

    public function createService()
    {
        $services = $this->servicesService->getAll();
        return view('services.create', ['services' => $services, 'user'=> Auth::user()]);
    }

    public function storeServices (Request $request)
    {
        $inputs = $request->all();
        $inputs['autonomous_id'] = Auth::id();
        
        if($this->servicesService->store($inputs)) {
            return redirect('/home')->with('msg', 'Serviço cadastrar com sucesso');;
        }
        return redirect('/home')->with('msgErro', 'Erro ao criar o serviço');
    }

    public function getServiceById(Request $request)
    {
        $id = $request->all()['id'];
        $services = $this->servicesService->getAll();
        $service = $this->servicesService->getByid($id)[0];

        return view('services.update',['service' => $service, 'allServices' => $services, 'user'=> Auth::user()]);
    }

    public function update(Request $request, $id)
    {
        $inputs = $request->all();

        if ($this->servicesService->update($inputs, $id)) {
           return redirect('/home')->with('msg', 'Serviço atualizado com sucesso');
        }

        return redirect('/home')->with('msgErro', 'Erro ao atualizar o serviço');
    }

    public function listaServicos()
    {
        $services = $this->servicesService->getAll();
        return view('services.list', ['services' => $services, 'user'=> Auth::user()]);
    }
}
