<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\ServicesServices;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $userService;

    protected $serviceService;

    public function __construct(UserService $userService, ServicesServices $servicesServices)
    {
        $this->userService = $userService;
        $this->serviceService = $servicesServices;
    }

    public function turnAutonomous()
    {
        return view('user.turnAutonomous', ['user'=> Auth::user()]);
    }

    public function updateToAutonomous(Request $request)
    {
        $inputs = $request->all();
        $inputs['role_id'] = 2;
        if($this->userService->update($inputs, Auth::id())) {
            return redirect('/home')->with('msg', 'Você torno-se um autonomo agora');
        }
        return redirect('/home')->with('msgErro','Erro ao se tornar Autonomo');
    }

    public function getUserById()
    {
        $user = $this->userService->getUserById(Auth::id());
        return view('/user/edit', ['users' => $user, 'user'=> Auth::user()]);
    }

    public function update(Request $request)
    {
        $inputs = $request->all();
        $id = $inputs['id'];
        unset($inputs['id']);

        if($this->userService->update($inputs, $id)) {
            return redirect('/home')->with('msg', 'atualização feita com sucecsso');
        }
        return redirect('/home')->with('msgErro', 'Erro ao atualizar o usuario');
    }

    public function getAutonomoByServiceId(Request $request)
    {
        $inputs = $request->all();
        $autonomous = $this->userService->getUserByServiceId($inputs['service_id']);
        return view('user.listAutonomous', ['autonomous' => $autonomous, 'date' => $inputs['date'], 'user'=> Auth::user()]);
    }

    public function getSchedules(Request $request)
    {
        $inputs = $request->all();
        $freeTime = $this->userService->timeFree($inputs['idAutonomo'], $inputs['date'], $inputs['serviceId']);
        $service = $this->serviceService->getByServiceId($inputs['serviceId'], $inputs['idAutonomo'])[0];

        return view('schedule.create', [
            'freeTimes'=> $freeTime,
            'service' => $service,
            'user'=> Auth::user(),
            'date' =>$inputs['date']
        ]);
    }
}