<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Services\ScheduleServices;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    protected $userService;
    protected $scheduleServices;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserService $userService, ScheduleServices $scheduleServices)
    {
        $this->userService = $userService;
        $this->scheduleServices = $scheduleServices;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schedulesClient     = $this->scheduleServices->getScheduleByUserId(Auth::id());
        $schedulesAutonomous = $this->scheduleServices->getScheduleByAutonomousId(Auth::id());
        $user = $this->userService->getUserById(Auth::id());
        return view('home', [
            'user' => $user,
            'schedulesClient' => $schedulesClient,
            'schedulesAutonomous' => $schedulesAutonomous
        ]);
    }
}
