<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutonomousServices extends Model
{
    protected $fillable = [
        'autonomous_id', 'service_id', 'time', 'value'
    ];
}
