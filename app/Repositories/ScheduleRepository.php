<?php

namespace App\Repositories;

use App\Schedules;
use Caffeinated\Repository\Repositories\EloquentRepository;

class ScheduleRepository extends EloquentRepository
{
    protected $schedulesModel;

    public function __construct(Schedules $schedules)
    {
        $this->schedulesModel = $schedules;
    }

    public function getByAutonomousId($autonomosId, $date)
    {
         return $this->schedulesModel
            ->where('autonomous_id', '=', $autonomosId)
            ->where('date', '=', $date)
            ->get()->all();
    }

    public function create($data)
    {
        return $this->schedulesModel->create($data);
    }

    public function getByUserId($userId)
    {
        return $this->schedulesModel
            ->select(
                'users.first_name',
                'users.last_name',
                'services.name',
                'schedules.date',
                'schedules.time'
            )
            ->where('client_id', '=', $userId)
            ->join('users', 'users.id', 'schedules.autonomous_id')
            ->join('services', 'services.id', 'schedules.service_id')
            ->get()->all();
    }

    public function getScheduleByAutonomousId($userId)
    {
        return $this->schedulesModel
            ->select(
                'users.first_name',
                'users.last_name',
                'services.name',
                'schedules.date',
                'schedules.time'
            )
            ->where('autonomous_id', '=', $userId)
            ->join('users', 'users.id', 'schedules.client_id')
            ->join('services', 'services.id', 'schedules.service_id')
            ->get()->all();
    }
}