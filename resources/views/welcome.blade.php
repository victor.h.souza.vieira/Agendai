    @extends('layouts.app')

    @section('content')

    <header class="masthead">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-lg-7 my-auto">
                    <div class="header-content mx-auto">
                        <h1 class="mb-5">
                            A agendaí é uma ferramenta focada em agilizar e organizar os agendamentos
                            de sua empresa ou de seu negócio!
                        </h1>
                        <a href="{{ route('register') }}" class="btn btn-outline btn-xl js-scroll-trigger">Teste Gratuito</a>
                    </div>
                </div>
                <div class="col-lg-5 my-auto">
                    <div class="device-container">
                        <div class="device-mockup iphone6_plus portrait white">
                            <div class="device">
                                <div class="screen">
                                    <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                                    <img src="{{ asset('images/img/agendai-app.png') }}" class="img-fluid" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="download bg-primary text-center" id="download">
        <div class="container">
            <div class="row">
                <div class="col-md-10 mx-auto">
                    <h2 class="section-heading">Baixe nosso aplicativo!</h2>
                    <p>Nosso aplicativo está disponível para download! <br>Faça o download agora!</p>
                    <div class="badges">
                        <a class="badge-link" href="#">
                            <img src="{{ asset('images/img/google-play-badge.svg') }}" alt="Baixe nosso aplicativo para Android">
                        </a>
                        <a class="badge-link" href="#">
                            <img src="{{ asset('images/img/app-store-badge.svg') }}" alt="Baixe nosso aplicativo para IOs">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="features" id="features">
        <div class="container">
            <div class="section-heading text-center">
                <h2>Funcionalidades da nossa ferramenta</h2>
                <p class="text-muted">Tanto na web quanto no seu dispositivo móvel, acesse todas as funcionalidades!</p>
                <hr>
            </div>
            <div class="row">
                <div class="col-lg-4 my-auto">
                    <div class="device-container">
                        <div class="device-mockup iphone6_plus portrait white">
                            <div class="device">
                                <div class="screen">
                                    <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                                    <img src="{{ asset('images/img/agendai-app.png') }}" class="img-fluid" alt="Aplicativo Mobile e Responsivo.">
                                </div>
                                <div class="button">
                                    <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 my-auto">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="feature-item">
                                    <i class="fas fa-mobile-alt"></i>
                                    <h3>Totalmente Responsivo</h3>
                                    <p class="text-muted">Acesse a Agendaí de seu smartphone, notebook ou computador desktop.</p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="feature-item">
                                    <i class="fas fa-calendar-alt"></i>
                                    <h3>Agendamento e Organizado</h3>
                                    <p class="text-muted">Organize seus agendamentos totalmente online e sem complicações!</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="feature-item">
                                    <i class="fas fa-rocket"></i>
                                    <h3>Agendamentos Rápidos</h3>
                                    <p class="text-muted">Realize um agendamento com rapidez e qualidade!</p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="feature-item">
                                    <i class="fas fa-check"></i>
                                    <h3>Avaliações dos Serviços</h3>
                                    <p class="text-muted">Avaliações sobre o serviço prestado pelos profissionais cadastrados no Agendaí!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="cta" id="cta-section">
        <div class="cta-content">
            <div class="container">
                <h2>Chega de esperar.<br>Agende agora!</h2>
                <a href="{{ route('register') }}" class="btn btn-outline btn-xl js-scroll-trigger">Agendar Agora</a>
            </div>
        </div>
        <div class="overlay"></div>
    </section>

    <section class="team" id="team">
        <div class="container">
            <div class="section-heading text-center">
                <h2>Nosso Time de Profissionais</h2>
                <p class="text-muted">Conheça nosso incrível time de profissionais.</p>
                <hr>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="team-member">
                        <img class="mx-auto rounded-circle" src="{{ asset('images/img/team/joao.jpg') }}" alt="">
                        <h4>João Vitor</h4>
                        <p class="text-muted">Desenvolvedor Mobile</p>
                        <ul class="list-inline social-buttons">
                            <li class="list-inline-item">
                                <a href="#" class="social-icon">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="social-icon">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="social-icon">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="team-member">
                        <img class="mx-auto rounded-circle" src="{{ asset('images/img/team/leonardo.jpg') }}" alt="">
                        <h4>Leonardo</h4>
                        <p class="text-muted">Desenvolvedor Back-End</p>
                        <ul class="list-inline social-buttons">
                            <li class="list-inline-item">
                                <a href="#" class="social-icon">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="social-icon">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="social-icon">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="team-member">
                        <img class="mx-auto rounded-circle" src="{{ asset('images/img/team/victor.jpg') }}" alt="">
                        <h4>Victor Hugo</h4>
                        <p class="text-muted">Desenvolvedor Front-End</p>
                        <ul class="list-inline social-buttons">
                            <li class="list-inline-item">
                                <a href="#" class="social-icon">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="social-icon">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="social-icon">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="team-member">
                        <img class="mx-auto rounded-circle" src="{{ asset('images/img/team/lucas.jpg') }}" alt="">
                        <h4>Lucas Costa</h4>
                        <p class="text-muted">Analista de Produtos</p>
                        <ul class="list-inline social-buttons">
                            <li class="list-inline-item">
                                <a href="#" class="social-icon">
                                    <i class="fab fa-facebook-f"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="social-icon">
                                    <i class="fab fa-linkedin-in"></i>
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#" class="social-icon">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 mx-auto text-center">
                    <p class="large text-muted">
                        A Agendaí é composta por uma equipe de amantes de técnologia, inovação e conhecimento.
                        Nos acompanhe nas redes sociais.
                    </p>
                </div>
            </div>
        </div>
    </section>

    @endsection
