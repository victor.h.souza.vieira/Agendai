@extends('layouts.dashboard')

@section('content')
@if($user->role_id == 1)
        <div class="col-md-6 col-sm-6 col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="h2">Meus Agendamentos</h4>
                    <p class="card-description card-title">Sessão do Cliente</p>
                    <div class="preview-list">
                        @foreach($schedulesClient as $schedule)
                            <div class="preview-item border-bottom px-0">
                                <div class="preview-thumbnail">
                                    <img src="{{ asset('images/img/users/user.svg') }}" alt="image" class="rounded-circle"/>
                                </div>
                                <div class="preview-item-content d-flex flex-grow">
                                    <div class="flex-grow">
                                        <h6 class="preview-subject">{{$schedule->first_name . ' ' . $schedule->last_name}}
                                            <span class="float-right small text-right">
                                                <span class="text-muted pr-3">{{Carbon\Carbon::parse($schedule->time)->format('H:i')}}</span>
                                                <br><br>
                                                <span class="text-muted pr-3">{{$schedule->date->format('d/m/Y')}}</span>
                                            </span>
                                        </h6>
                                        <p>{{$schedule->name}}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @elseif($user->role_id == 2 )
        <div class="col-md-6 col-sm-6 col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="h2">Minha Agenda</h4>
                    <p class="card-description card-title">Sessão do Autonomo</p>
                    <div class="preview-list">
                        @foreach($schedulesAutonomous as $schedule)
                            <div class="preview-item border-bottom px-0">
                                <div class="preview-thumbnail">
                                    <img src="{{ asset('images/img/users/user.svg') }}" alt="image" class="rounded-circle"/>
                                </div>
                                <div class="preview-item-content d-flex flex-grow">
                                    <div class="flex-grow">
                                        <h6 class="preview-subject">{{$schedule->first_name . ' ' . $schedule->last_name}}
                                            <span class="float-right small text-right">
                                                <span class="text-muted pr-3">{{Carbon\Carbon::parse($schedule->time)->format('H:i')}}</span>
                                                <br><br>
                                                <span class="text-muted pr-3">{{$schedule->date->format('d/m/Y')}}</span>
                                            </span>
                                        </h6>
                                        <p>{{$schedule->name}}</p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="col-md-6 col-sm-6 col-lg-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="h2">Meus Agendamentos</h4>
                        <p class="card-description card-title">Sessão do Cliente</p>
                        <div class="preview-list">
                            <div class="preview-item border-bottom px-0">
                                <div class="preview-thumbnail">
                                    <img src="{{ asset('images/img/users/user.svg') }}" alt="image" class="rounded-circle"/>
                                </div>
                                <div class="preview-item-content d-flex flex-grow">
                                    <div class="flex-grow">
                                        <h6 class="preview-subject">MM - Maria Manicure
                                            <span class="float-right small text-right">
                                            <span class="text-muted pr-3">11:00</span>
                                            <br><br>
                                            <span class="text-muted pr-3">21-06-2018</span>
                                        </span>
                                        </h6>
                                        <p>Manicure</p>
                                    </div>
                                </div>
                            </div>
                            <div class="preview-item border-bottom px-0">
                                <div class="preview-thumbnail">
                                    <img src="{{ asset('images/img/users/user.svg') }}" alt="image" class="rounded-circle"/>
                                </div>
                                <div class="preview-item-content d-flex flex-grow">
                                    <div class="flex-grow">
                                        <h6 class="preview-subject">Pedro Cabelereiro
                                            <span class="float-right small text-right">
                                            <span class="text-muted pr-3">11:00</span>
                                            <br><br>
                                            <span class="text-muted pr-3">21-06-2018</span>
                                        </span>
                                        </h6>
                                        <p>Corte de cabelo e barba</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="col-md-6 col-sm-6 col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="h2">Minha Agenda</h4>
                    <p class="card-description card-title">Sessão do Autonomo</p>
                    <div class="preview-list">
                        <div class="preview-item border-bottom px-0">
                            <div class="preview-thumbnail">
                                <img src="{{ asset('images/img/users/user.svg') }}" alt="image" class="rounded-circle"/>
                            </div>
                            <div class="preview-item-content d-flex flex-grow">
                                <div class="flex-grow">
                                    <h6 class="preview-subject">Cliente: <span class="text-orange-theme">Nome do Cliente</span>
                                        <span class="float-right small text-right">
                                        <span class="text-muted pr-3">11:00</span>
                                        <br><br>
                                        <span class="text-muted pr-3">21-06-2018</span>
                                    </span>
                                    </h6>
                                    <p>Descrição do Serviço Agendado</p>
                                </div>
                            </div>
                        </div>
                        <div class="preview-item border-bottom px-0">
                            <div class="preview-thumbnail">
                                <img src="{{ asset('images/img/users/user.svg') }}" alt="image" class="rounded-circle"/>
                            </div>
                            <div class="preview-item-content d-flex flex-grow">
                                <div class="flex-grow">
                                    <h6 class="preview-subject">Cliente: <span class="text-orange-theme">Nome do Cliente</span>
                                        <span class="float-right small text-right">
                                        <span class="text-muted pr-3">12:00</span>
                                        <br><br>
                                        <span class="text-muted pr-3">21-06-2018</span>
                                    </span>
                                    </h6>
                                    <p>Descrição do Serviço Agendado</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
