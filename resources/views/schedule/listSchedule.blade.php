@extends('layouts.dashboard')
@section('content')
<div style="display:none;" class="container">
    <h3>Autonomos que prestão esse serviço:</h3>
    <br>
    <div class="row">
        @foreach($autonomous as $autonomou)

            <div class="col-sm-4">
                <div class="card">
                    <div class="card-header">
                        {{$autonomou['first_name'] . ' ' .  $autonomou['last_name']}}
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{$autonomou['name']}}</h5>
                        <p class="card-text">
                            <span>Valor: {{$autonomou['value']}}</span></p>
                        <p class="card-text">
                            <span>Tempo para realizar o serviço: {{$autonomou['value']}}</span>
                        </p>
                        <p class="card-text">
                            <span>Telefone: {{$autonomou['telephone']}}</span>
                        </p>
                        <a href="{{route('horariosAutonomo', [
                                        'idAutonomo' => $autonomou['userId'],
                                        'date' => $date,
                                        'serviceId' => $autonomou['serviceId']])}}" class="btn btn-primary btn-block">Agendar</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@endsection