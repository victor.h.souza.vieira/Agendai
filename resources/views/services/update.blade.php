@extends('layouts.dashboard')
@section('content')
<div class="col-md-12 col-sm-12 col-lg-12 d-flex align-items-stretch grid-margin">
    <div class="row flex-grow">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="h2">Editar Serviço</h4>
                    <hr>
                    <br>
                    <form class="forms-sample" action="{{route('updateServico', ['id' => $service->autonomous_serviceId])}}" method="post">

                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="">

                        <div class="row">

                            <div class="col-sm-12 col-md-5 col-lg-5">
                                <div class="form-group">
                                    <label for="time">Serviço</label>
                                    <div class="form-group">
                                        <label for="service_id">Tipo de Serviço</label>
                                        <select class="form-control" name="service_id" id="service_id">
                                            @foreach($allServices as $serviceList)
                                                <option value="{{$serviceList->id}}"
                                                            @if($serviceList->id == $service->id)
                                                            selected
                                                            @endif
                                                    >{{$serviceList->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="value">Preço</label>
                                    <input type="text" class="form-control money3" maxlength="8" name="value" value="{{$service->value}}">
                                </div>

                                <div class="form-group">
                                    <label for="value">Duração <span class="sub-text-table">(em minutos)</span></label>
                                    <input type="text" class="form-control" name="time" value="{{$service->time}}">
                                </div>

                                <div class="row mt-4">
                                    <div class="col-sm-4 col-md-4 col-lg-4">
                                        <button type="submit" class="btn btn-success mr-2">Atualizar</button>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection