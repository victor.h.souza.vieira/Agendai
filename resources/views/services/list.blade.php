@extends('layouts.dashboard')
@section('content')
<div class="col-md-12 col-sm-12 col-lg-12 d-flex align-items-stretch grid-margin">
    <div class="row flex-grow">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="h2">Selecione o Tipo de Serviço</h4>
                    <hr>
                    <br>
                    <form class="forms-sample" action="{{route('getAutonomoByServiceId')}}" method="GET">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="service_id">Tipo de Serviço</label>
                            <select class="form-control" name="service_id" id="service_id">
                                @foreach($services as $service)
                                    <option value="{{$service->id}}">{{$service->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="time">Data</label>
                            <input type="date" class="form-control" name="date" id="date" placeholder="Digite uma data ou clique na seta ao lado" required>
                        </div>

                        <div class="row mt-4">
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <button type="submit" class="btn btn-success mr-2">Buscar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection