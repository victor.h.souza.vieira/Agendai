@extends('layouts.dashboard')

@section('content')
<div class="col-md-12 col-sm-12 col-lg-12 d-flex align-items-stretch grid-margin">
    <div class="row flex-grow">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="h2">Cadatrar Serviço</h4>
                    <hr>
                    <br>
                    <form class="forms-sample" action="{{route('storeServices')}}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="service_id">Selecione um tipo de serviço</label>
                            <select class="form-control" name="service_id" id="service_id">
                                @foreach($services as $service)
                                    <option value="{{$service->id}}">{{$service->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="time">Duração do Serviço (em minutos)</label>
                            <input type="text" class="form-control" name="time" id="time" required>
                        </div>
                        <div class="form-group">
                            <label for="value">Valor do Serviço</label>
                            <input type="text" class="form-control" name="value" id="value" required>
                        </div>
                        <div class="row mt-4">
                            <div class="col-sm-4 col-md-4 col-lg-4">
                                <a href="{{route('cadastrarServico')}}">
                                    <button type="submit" class="btn btn-success mr-2">Cadastrar</button>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection